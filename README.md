# ToyRobot Simulator

This is a command line application is a simulation of a toy robot on a 5x5 tabletop. User can interact with the toy robot using specific commands in the command line.

## Environment

Application was developed using Ruby 2.7.

## Installation

To install the gem clone the repo to your local machine and:

```
cd toy_robot
bin/setup
bundle exec rake install
```
## Testing

Run tests using
```
bundle exec rspec
```

## Usage

Executable is available for normal usage.

```
bin/start
```
Valid commands are:

| Command       | Description
| ---           | ---
| `PLACE x,y,d` | Places robot at position `x`, `y`, facing direction `d`. E.g. `PLACE 1,2,NORTH`.
| `LEFT`        | Rotates robot 90° counter-clockwise.
| `RIGHT`       | Rotates robot 90° clockwise.
| `MOVE`        | Advances the robot one position in the direction it is currently facing.
| `REPORT`      | Prints the current location.
| `EXIT`        | Closes the application.

Commands resulting in the robot moving to an out-of-bounds position (`x` or `y` being less than 0 or greater than 4) will be ignored.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

The app implements [command pattern](https://en.wikipedia.org/wiki/Command_pattern).

## Specification

Application was written in order to fulfil following specifications:

## Description

- The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement
  that would result in the robot falling from the table must be prevented, however further valid movement commands must still
  be allowed.

Create an application that can read in commands of the following form:

```plain
PLACE X,Y,F
MOVE
LEFT
RIGHT
REPORT
```

- PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.
- REPORT will announce the X,Y and orientation of the robot.
- A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.
- Provide test data to exercise the application.

## Constraints

The toy robot must not fall off the table during movement. This also includes the initial placement of the toy robot.
Any move that would cause the robot to fall must be ignored.

Example Input and Output:

```plain
PLACE 0,0,NORTH
MOVE
REPORT
Output: 0,1,NORTH
```

```plain
PLACE 0,0,NORTH
LEFT
REPORT
Output: 0,0,WEST
```

```plain
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
Output: 3,3,NORTH
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
