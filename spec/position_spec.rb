# frozen_string_literal: true

RSpec.describe ToyRobot::Position do
  describe '#go_to' do
    it 'goes NORTH' do
      position = described_class.new(2, 1, 'NORTH')

      expect(position.go_to('NORTH'))
        .to eq(described_class.new(2, 2, 'NORTH'))
    end

    it 'goes SOUTH' do
      position = described_class.new(2, 1, 'SOUTH')

      expect(position.go_to('SOUTH'))
        .to eq(described_class.new(2, 0, 'SOUTH'))
    end

    it 'goes EAST' do
      position = described_class.new(2, 1, 'EAST')

      expect(position.go_to('EAST'))
        .to eq(described_class.new(3, 1, 'EAST'))
    end

    it 'goes WEST' do
      position = described_class.new(2, 1, 'WEST')

      expect(position.go_to('WEST'))
        .to eq(described_class.new(1, 1, 'WEST'))
    end
  end

  describe '#==' do
    it 'returns true when coordinantes are the same in both positions' do
      position_one = described_class.new(1, 1, 'NORTH')
      position_two = described_class.new(1, 1, 'NORTH')

      expect(position_one).to eq(position_two)
    end
  end

  describe '#direction_left' do
    it 'when facing NORTH returns WEST' do
      position = described_class.new(1, 2, 'NORTH')

      expect(position.direction_left).to eq('WEST')
    end

    it 'when facing WEST returns SOUTH' do
      position = described_class.new(1, 2, 'WEST')

      expect(position.direction_left).to eq('SOUTH')
    end

    it 'when facing SOUTH returns EAST' do
      position = described_class.new(1, 2, 'SOUTH')

      expect(position.direction_left).to eq('EAST')
    end

    it 'when facing EAST returns NORTH' do
      position = described_class.new(1, 2, 'EAST')

      expect(position.direction_left).to eq('NORTH')
    end
  end

  describe '#direction_right' do
    it 'when facing NORTH returns EAST' do
      position = described_class.new(1, 2, 'NORTH')

      expect(position.direction_right).to eq('EAST')
    end

    it 'when facing EAST returns SOUTH' do
      position = described_class.new(1, 2, 'EAST')

      expect(position.direction_right).to eq('SOUTH')
    end

    it 'when facing SOUTH returns WEST' do
      position = described_class.new(1, 2, 'SOUTH')

      expect(position.direction_right).to eq('WEST')
    end

    it 'when facing WEST returns NORTH' do
      position = described_class.new(1, 2, 'SOUTH')

      expect(position.direction_right).to eq('WEST')
    end
  end
end
