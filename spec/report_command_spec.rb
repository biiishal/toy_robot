# frozen_string_literal: true

RSpec.describe ToyRobot::ReportCommand do
  describe '#execute' do
    let(:robot) { ToyRobot::Robot.new }
    let(:table) { ToyRobot::Table.new(5, 5) }

    context 'with valid position' do
      let!(:position) { ToyRobot::Position.new(1, 1, 'EAST') }

      it 'prints report to stdout' do
        ToyRobot::PlaceCommand.new(robot, table, position).execute

        expected_report = "1,1,EAST\n"

        expect { described_class.new(robot).execute }.to output(expected_report).to_stdout
      end
    end

    context 'with invalid position' do
      let!(:position) { ToyRobot::Position.new(-1, 1, 'EAST') }

      it 'prints report to stdout' do
        ToyRobot::PlaceCommand.new(robot, table, position).execute

        expected_report = "not in place\n"

        expect { described_class.new(robot).execute }.to output(expected_report).to_stdout
      end
    end
  end
end
