# frozen_string_literal: true

RSpec.describe ToyRobot::LeftCommand do
  describe '#execute' do
    let(:robot) { ToyRobot::Robot.new }
    let(:table) { ToyRobot::Table.new(5, 5) }
    let(:left_command) { described_class.new(robot) }

    it 'when facing NORTH turns WEST' do
      position = ToyRobot::Position.new(1, 1, 'NORTH')

      ToyRobot::PlaceCommand.new(robot, table, position).execute

      left_command.execute

      expect(robot.report).to eq('1,1,WEST')
    end

    it 'when facing WEST turns SOUTH' do
      position = ToyRobot::Position.new(1, 1, 'WEST')

      ToyRobot::PlaceCommand.new(robot, table, position).execute

      left_command.execute

      expect(robot.report).to eq('1,1,SOUTH')
    end

    it 'when facing SOUTH turns EAST' do
      position = ToyRobot::Position.new(1, 1, 'SOUTH')

      ToyRobot::PlaceCommand.new(robot, table, position).execute

      left_command.execute

      expect(robot.report).to eq('1,1,EAST')
    end

    it 'when facing EAST turns NORTH' do
      position = ToyRobot::Position.new(1, 1, 'EAST')

      ToyRobot::PlaceCommand.new(robot, table, position).execute

      left_command.execute

      expect(robot.report).to eq('1,1,NORTH')
    end
  end
end
