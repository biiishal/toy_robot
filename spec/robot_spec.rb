# frozen_string_literal: true

RSpec.describe ToyRobot::Robot do
  let(:robot) { described_class.new }
  let(:table) { ToyRobot::Table.new(5, 5) }

  describe '#report' do
    let(:position) { ToyRobot::Position.new(2, 3, 'NORTH') }

    context 'after inicial placement of the robot' do
      it 'reports current position correctly' do
        ToyRobot::PlaceCommand.new(robot, table, position).execute

        expect(robot.report).to eq('2,3,NORTH')
      end
    end

    context 'before initial placement of the robot' do
      it 'reports error message' do
        expect(robot.report).to eq('not in place')
      end
    end
  end

  describe '#report' do
    it 'returns a short report' do
      position = ToyRobot::Position.new(1, 1, 'NORTH')

      ToyRobot::PlaceCommand.new(robot, table, position).execute

      expect(robot.report).to eq('1,1,NORTH')
    end
  end

  describe '#placed?' do
    context 'before robot is placed' do
      it 'returns false' do
        expect(robot.placed?).to be false
      end
    end

    context 'after robot is placed' do
      it 'returns true' do
        ToyRobot::PlaceCommand.new(robot, table, ToyRobot::Position.new(1, 2, 'EAST')).execute

        expect(robot.placed?).to be true
      end
    end
  end
end
