# frozen_string_literal: true

RSpec.describe ToyRobot::MoveCommand do
  describe '#execute' do
    let(:robot) { ToyRobot::Robot.new }
    let(:table) { ToyRobot::Table.new(5, 5) }
    let(:move_command) { described_class.new(robot, table) }

    it 'decreases x when robot is facing WEST' do
      position = ToyRobot::Position.new(2, 4, 'WEST')
      ToyRobot::PlaceCommand.new(robot, table, position).execute
      move_command.execute
      expect(robot.report).to eq('1,4,WEST')
    end

    it 'increases x when robot is facing EAST' do
      position = ToyRobot::Position.new(3, 4, 'EAST')
      ToyRobot::PlaceCommand.new(robot, table, position).execute
      move_command.execute
      expect(robot.report).to eq('4,4,EAST')
    end

    it 'increases y when robot is facing NORTH' do
      position = ToyRobot::Position.new(2, 3, 'NORTH')
      ToyRobot::PlaceCommand.new(robot, table, position).execute
      move_command.execute
      expect(robot.report).to eq('2,4,NORTH')
    end

    it 'decreases y when robot is facing SOUTH' do
      position = ToyRobot::Position.new(3, 3, 'SOUTH')
      ToyRobot::PlaceCommand.new(robot, table, position).execute
      move_command.execute
      expect(robot.report).to eq('3,2,SOUTH')
    end

    it 'does nothing when position is invalid' do
      position = ToyRobot::Position.new(5, 5, 'NORTH')
      ToyRobot::PlaceCommand.new(robot, table, position).execute
      move_command.execute
      expect(robot.report).to eq('5,5,NORTH')
    end
  end
end
