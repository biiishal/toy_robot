# frozen_string_literal: true

RSpec.describe ToyRobot::PlaceCommand do
  describe '#execute' do
    let(:robot) { ToyRobot::Robot.new }
    let(:table) { ToyRobot::Table.new(5, 5) }
    let(:place_command) { described_class.new(robot, table, position) }

    context 'with valid position' do
      let(:position) { ToyRobot::Position.new(2, 3, 'NORTH') }

      it 'places robot ' do
        place_command.execute

        expect(robot.report).to eq('2,3,NORTH')
      end
    end

    context 'with INVALID position' do
      let(:position) { ToyRobot::Position.new(-1, 1, 'NORTH') }

      it 'places robot ' do
        place_command.execute

        expect(robot.report).to eq('not in place')
      end
    end
  end
end
