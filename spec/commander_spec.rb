# frozen_string_literal: true

RSpec.describe ToyRobot::Commander do
  let(:robot) { ToyRobot::Robot.new }
  let(:table) { ToyRobot::Table.new(5, 5) }
  let(:commander) { described_class.new(robot, table) }

  describe '#parse' do
    context 'PLACE command' do
      input = 'PLACE 1, 1, NORTH'

      it 'creates command with the correct arguments' do
        position = instance_double('ToyRobot::Position', x: 1, y: 1, direction: 'NORTH')

        expect(ToyRobot::Position).to receive(:new).with(1, 1, 'NORTH').and_return(position)

        expect(ToyRobot::PlaceCommand).to receive(:new).with(robot, table, position)

        commander.parse(input)
      end

      it 'returns command' do
        expect(commander.parse(input)).to be_a ToyRobot::PlaceCommand
      end
    end

    context 'MOVE command' do
      input = 'MOVE'

      it 'creates command with the correct arguments' do
        expect(ToyRobot::MoveCommand).to receive(:new).with(robot, table)

        commander.parse(input)
      end

      it 'returns command' do
        expect(commander.parse(input)).to be_a ToyRobot::MoveCommand
      end
    end

    context 'LEFT command' do
      input = 'LEFT'

      it 'creates command with the correct arguments' do
        expect(ToyRobot::LeftCommand).to receive(:new).with(robot)

        commander.parse(input)
      end

      it 'returns command' do
        expect(commander.parse(input)).to be_a ToyRobot::LeftCommand
      end
    end

    context 'RIGHT command' do
      input = 'RIGHT'

      it 'creates command with the correct arguments' do
        expect(ToyRobot::RightCommand).to receive(:new).with(robot)

        commander.parse(input)
      end

      it 'returns command' do
        expect(commander.parse(input)).to be_a ToyRobot::RightCommand
      end
    end

    context 'REPORT command' do
      input = 'REPORT'

      it 'creates command with the correct arguments' do
        expect(ToyRobot::ReportCommand).to receive(:new).with(robot)

        commander.parse(input)
      end

      it 'returns command' do
        expect(commander.parse(input)).to be_a ToyRobot::ReportCommand
      end
    end

    context 'INVALID command' do
      it 'returns nil' do
        input = 'asdfjoaifjçj'

        expect(commander.parse(input)).to be nil
      end
    end
  end
end
