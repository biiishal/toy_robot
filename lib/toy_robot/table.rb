# frozen_string_literal: true

module ToyRobot
  # The table where the robot is placed and moves.
  class Table
    def initialize(length, width)
      @length = length
      @width = width
    end

    def position_is_valid?(position)
      position.x <= @length &&
        position.y <= @width &&
        position.x >= 0 &&
        position.y >= 0
    end
  end
end
