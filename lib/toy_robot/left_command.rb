# frozen_string_literal: true

module ToyRobot
  # LeftCommand will change the direction faced by robot to Left 
  # with position remaining same.
  class LeftCommand
    def initialize(robot)
      @robot = robot
    end

    def execute
      return unless @robot.placed?
      current_position = @robot.current_position
      @robot.current_position = Position.new(current_position.x,
                                             current_position.y,
                                             current_position.direction_left)
    end
  end
end
