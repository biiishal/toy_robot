# frozen_string_literal: true

module ToyRobot
  class ReportCommand
    def initialize(robot)
      @robot = robot
    end

    def execute
      puts @robot.report
    end
  end
end
