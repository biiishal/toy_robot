# frozen_string_literal: true

module ToyRobot
  # RightCommand will change the direction faced by robot to right
  # with position remaining same.
  class RightCommand
    def initialize(robot)
      @robot = robot
    end

    def execute
      return unless @robot.placed?
      current_position = @robot.current_position
      @robot.current_position = Position.new(current_position.x,
                                             current_position.y,
                                             current_position.direction_right)
    end
  end
end
