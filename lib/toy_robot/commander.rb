# frozen_string_literal: true

module ToyRobot
  # parses user input and returns subsequent command object.
  class Commander
    def initialize(robot, table)
      @robot = robot
      @table = table
    end

    def parse(command)
      if command =~ /^PLACE\s+\d+\s*,\s*\d+\s*,\s*(NORTH|SOUTH|EAST|WEST)$/
        _command, x, y, direction = command.tr(',', ' ').split

        PlaceCommand.new(@robot, @table, Position.new(x.to_i, y.to_i, direction))
      elsif command =~ /^MOVE$/
        MoveCommand.new(@robot, @table)
      elsif command =~ /^LEFT$/
        LeftCommand.new(@robot)
      elsif command =~ /^RIGHT$/
        RightCommand.new(@robot)
      elsif command =~ /^REPORT$/
        ReportCommand.new(@robot)
      end
    end
  end
end
