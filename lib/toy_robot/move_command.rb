# frozen_string_literal: true

module ToyRobot
  # MoveCommand will move robot one step more towards the direction it's facing.
  # If new position is invalid does nothing.
  class MoveCommand
    def initialize(robot, table)
      @robot = robot
      @table = table
    end

    def execute
      return unless @robot.placed?
      new_position = @robot
                     .current_position
                     .go_to(@robot.current_position.direction)
      @robot.current_position = new_position if @table.position_is_valid?(new_position)
    end
  end
end
