# frozen_string_literal: true

module ToyRobot
  # Position of the robot in the table. x, y and direction the toy is facing.
  # Based on the normal co-ordinates where origin is at (0, 0), South-West corner.
  class Position
    attr_reader :x, :y, :direction

    DIRECTIONS = %w[NORTH EAST SOUTH WEST].freeze

    def initialize(x, y, direction)
      @x = x
      @y = y
      @direction = direction
    end

    def go_to(direction)
      case direction
      when 'EAST'
        go_east
      when 'WEST'
        go_west
      when 'NORTH'
        go_north
      when 'SOUTH'
        go_south
      end
    end

    def ==(other)
      @x == other.x && @y == other.y && @direction == other.direction
    end

    def direction_left
      DIRECTIONS[(DIRECTIONS.index(direction) - 1) % 4]
    end

    def direction_right
      DIRECTIONS[(DIRECTIONS.index(direction) + 1) % 4]
    end

    private

    def go_east
      Position.new(@x + 1, @y, @direction)
    end

    def go_west
      Position.new(@x - 1, @y, @direction)
    end

    def go_north
      Position.new(@x, @y + 1, @direction)
    end

    def go_south
      Position.new(@x, @y - 1, @direction)
    end
  end
end
