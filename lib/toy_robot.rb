# frozen_string_literal: true

%w[version
   position
   table
   robot
   place_command
   report_command
   move_command
   left_command
   right_command
   commander].each do |file|
  require_relative "./toy_robot/#{file}.rb"
end

# Main module for the app.
module ToyRobot
  # Entry point
  class Simulator
    def self.start
      table = Table.new(5, 5)
      robot = Robot.new
      commander = Commander.new(robot, table)

      puts 'Welcome to Toy Robotics.'
      puts 'Insert command RIGHT, LEFT, MOVE, REPORT or PLACE [X],[Y],[FACING_DIRECTION: can be EAST, WEST, NORTH or SOUTH] (eg: PLACE 2,3,EAST)'

      loop do
        puts 'Input Command (EXIT to Quit): '

        input = gets

        command = commander.parse(input)

        command.execute unless command.nil?

        break if input =~ /exit/i
      end
    end
  end
end
